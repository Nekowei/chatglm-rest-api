from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from transformers import AutoTokenizer, AutoModel

class Predict(APIView):   
    def get(self, request, input, format=None):
        print(input)
        res = chat(input)
        return Response(data = res)
    
    def post(self, request, input, history, format=None):
        print(input)
        print(history)
        res, his = chatWithHistory(input, history)
        return Response(data = {
            "response": res,
            "history": his
        })
    
def getModel():
    tokenizer = AutoTokenizer.from_pretrained("THUDM/chatglm-6b", trust_remote_code=True)
    model = AutoModel.from_pretrained("THUDM/chatglm-6b", trust_remote_code=True).half().cuda()
    return tokenizer, model

def chat(input):
    tokenizer, model = getModel()
    response, history = model.chat(tokenizer, input, history=[])
    return response

def chatWithHistory(input, history):
    tokenizer, model = getModel()
    response, history = model.chat(tokenizer, input, history)
    return response, history
