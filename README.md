# chatglm-rest-api

用于chatglm模型的api服务

使用django rest framework搭建

# 安装

请先确保已经安装了python环境（最好是最新版本的python3）

运行init.sh一键安装chatglm和django依赖

# 启动

运行startup.sh启动服务，默认端口6006，用于autodl外部访问
